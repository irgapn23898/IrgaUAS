# Tugas Akhir PWD Lumen

Ini adalah tugas akhir pwd saya berupa Aplikasi Dealer Motor , merupakan sebuah aplikasi simpel berbasis web yang dibuat menggunakan lumen untuk backend dan 
vue.js untuk frontend. Aplikasi digunakan untuk menyimpan data penting yang menyangkut hal hal tentang dealer motor.
Seperti : 
1. Data Motor
          
2. Data Sparepart
          
          
Untuk menggunakan aplikasi ini anda bisa mengclonenya.

Your lumen project must use localhost:8080;

Steps : 


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# UI APLIKASI

## 1. Beranda
Beranda ini digunakan sebagai identitas web dan hal pertama untuk menarik seseorang dalam sebuah web.
![Beranda](programaplikasi/1.png)

## 2. Read Data (Fitur Sparepart)
Sebuah page yang berisi data data sparepart yang didalamnya dapat melakukan sebuah crud.
![ReadData](programaplikasi/2.png)

## 3. Create Data (Fitur Sparepart)
Sebuah page form untuk melakukan sebuah penambahan data sparepart dan lansung bertambah kedatabase.
![ReadData](programaplikasi/3.png)

## 4. Edit Data (Fitur Sparepart)
Sebuah page form untuk melakukan sebuah Perubahan data sparepart dan lansung berubah didatabasenya pun .
![ReadData](programaplikasi/4.png)



