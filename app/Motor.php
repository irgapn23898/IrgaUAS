<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Motor extends Model
{

  public $table = 't_motor';

  protected $fillable = ['jenis_motor','harga','stok','merk'];

}
