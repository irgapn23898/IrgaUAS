<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Spare extends Model
{

  public $table = 't_sparepart';

  protected $fillable = ['sparepart','harga','stok','merk'];

}
