import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Motor from '@/components/Motor'
import MotorForm from '@/components/MotorForm'
import Spare from '@/components/Spare'
import SpareForm from '@/components/SpareForm'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/motor',
      name: 'Motor',
      component: Motor
    },
    {
      path: '/motor/create',
      name: 'MotorCreate',
      component: MotorForm
    },
    {
      path: '/motor/:id',
      name: 'MotorEdit',
      component: MotorForm
    },
    {
      path: '/spare',
      name: 'Spare',
      component: Spare
    },
    {
      path: '/spare/create',
      name: 'SpareCreate',
      component: SpareForm
    },
    {
      path: '/spare/:id',
      name: 'SpareEdit',
      component: SpareForm
    }
  ]
})
