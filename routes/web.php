<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//motor
$router->post('/motor','MotorController@create');
$router->get('/motor','MotorController@read');
$router->post('/motor/{id}','MotorController@update');
$router->delete('/motor/{id}','MotorController@delete');
$router->get('motor/{id}','MotorController@detail');

//Spare
$router->post('/spare','SpareController@create');
$router->get('/spare','SpareController@read');
$router->post('/spare/{id}','SpareController@update');
$router->delete('/spare/{id}','SpareController@delete');
$router->get('spare/{id}','SpareController@detail');